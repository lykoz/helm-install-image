FROM --platform=$TARGETPLATFORM alpine:3.9 AS base
ARG TARGETARCH
FROM base AS build-base
RUN apk add --no-cache curl

FROM build-base AS kubectl
ARG KUBECTL_VERSION
ARG SOURCE=https://dl.k8s.io/v$KUBECTL_VERSION/kubernetes-client-linux-$TARGETARCH.tar.gz
ARG TARGET=/kubernetes-client.tar.gz
RUN curl -fLSs "$SOURCE" -o "$TARGET"
RUN sha512sum "$TARGET"
RUN export KUBECTL_CHECKSUM=$(curl -fLSs https://dl.k8s.io/v$KUBECTL_VERSION/kubernetes-client-linux-$TARGETARCH.tar.gz.sha512); echo "$KUBECTL_CHECKSUM *$TARGET" | sha512sum -c -
RUN tar -xvf "$TARGET" -C /

FROM build-base AS helm
ARG HELM_VERSION
ARG SOURCE=https://get.helm.sh/helm-v$HELM_VERSION-linux-$TARGETARCH.tar.gz
ARG TARGET=/helm.tar.gz
RUN curl -fLSs "$SOURCE" -o "$TARGET"
RUN sha256sum "$TARGET"
RUN export HELM_CHECKSUM=$(curl -fLSs https://get.helm.sh/helm-v$HELM_VERSION-linux-$TARGETARCH.tar.gz.sha256); echo "$HELM_CHECKSUM *$TARGET" | sha256sum -c -
RUN mkdir -p /helm
RUN tar -xvf "$TARGET" -C /helm

FROM build-base AS stage
WORKDIR /stage
ENV PATH=$PATH:/stage/usr/bin
COPY --from=kubectl /kubernetes/client/bin/kubectl ./usr/bin/
# [ht]* is a hack to match helm and tiller, but not fail if tiller is not present. The tests will catch a missing helm binary.
COPY --from=helm /helm/linux-$TARGETARCH/[ht]* ./usr/bin/

FROM base
RUN apk add ca-certificates git
COPY --from=stage /stage/ /
