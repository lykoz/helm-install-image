FROM --platform=$TARGETPLATFORM alpine:3.9 AS base
ARG TARGETARCH
FROM base AS build-base
RUN apk add --no-cache curl

FROM build-base AS kubectl
ARG KUBECTL_VERSION
ARG SOURCE=https://dl.k8s.io/v$KUBECTL_VERSION/kubernetes-client-linux-$TARGETARCH.tar.gz
ARG TARGET=/kubernetes-client.tar.gz
RUN curl -fLSs "$SOURCE" -o "$TARGET"
RUN sha512sum "$TARGET"
RUN export KUBECTL_CHECKSUM=$(curl -fLSs https://dl.k8s.io/v$KUBECTL_VERSION/kubernetes-client-linux-$TARGETARCH.tar.gz.sha512); echo "$KUBECTL_CHECKSUM *$TARGET" | sha512sum -c -
RUN tar -xvf "$TARGET" -C /

FROM build-base AS helm
ARG HELM2_VERSION
ARG HELM3_VERSION

RUN curl -fLSs "https://get.helm.sh/helm-v$HELM2_VERSION-linux-$TARGETARCH.tar.gz" -o "/tmp/helm2.tar.gz" \
    && sha256sum "/tmp/helm2.tar.gz" \
    && export HELM2_CHECKSUM=$(curl -fLSs https://get.helm.sh/helm-v$HELM2_VERSION-linux-$TARGETARCH.tar.gz.sha256) \
    && echo "$HELM2_CHECKSUM */tmp/helm2.tar.gz" | sha256sum -c - \
    && mkdir -p /tmp/helm2 \
    && tar -xvf /tmp/helm2.tar.gz -C /tmp/helm2

RUN curl -fLSs "https://get.helm.sh/helm-v$HELM3_VERSION-linux-$TARGETARCH.tar.gz" -o "/tmp/helm3.tar.gz" \
    && sha256sum "/tmp/helm3.tar.gz" \
    && export HELM3_CHECKSUM=$(curl -fLSs https://get.helm.sh/helm-v$HELM3_VERSION-linux-$TARGETARCH.tar.gz.sha256) \
    && echo "$HELM3_CHECKSUM */tmp/helm3.tar.gz" | sha256sum -c - \
    && mkdir -p /tmp/helm3 \
    && tar -xvf /tmp/helm3.tar.gz -C /tmp/helm3

FROM build-base AS stage
WORKDIR /stage
COPY --from=kubectl /kubernetes/client/bin/kubectl ./usr/bin/
COPY --from=helm /tmp/helm2/linux-$TARGETARCH/helm ./usr/bin/helm2
COPY --from=helm /tmp/helm2/linux-$TARGETARCH/tiller ./usr/bin/tiller
COPY --from=helm /tmp/helm3/linux-$TARGETARCH/helm ./usr/bin/helm3

FROM base
RUN apk add --no-cache ca-certificates git
COPY --from=stage /stage/ /
RUN /usr/bin/helm3 plugin install https://github.com/helm/helm-2to3.git
